import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Slider;
import java.util.Observer;
import java.util.Observable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;

public class TipCalculatorView extends Application implements Observer {

    private static final NumberFormat CURRENCY = NumberFormat.getCurrencyInstance();
    private static final NumberFormat PERCENT = NumberFormat.getPercentInstance();

    static {
        CURRENCY.setRoundingMode(RoundingMode.HALF_UP);
    }

    private Label tipPercentLabel;
    private TextField tipField;
    private TextField totalField;
    private TipCalculatorModel model;

    public static void main(String[] args){
        Application.launch(args);
    }

    @Override
    public void start(Stage stage){
        stage.setTitle("Tip Calculator");

        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setVgap(10);
        //gridPane.setHGap(50);
        
        Scene scene = new Scene(gridPane, 250, 300);
        stage.setScene(scene);

        Label label1 = new Label("Amount On Bill:");
        gridPane.add(label1, 0, 0);

        TextField amountField = new TextField("0.00");
        // add listener to amount field
        amountField.textProperty().addListener(new ChangeListener<String>() {
                public void changed(ObservableValue<? extends String> observable,
                                        String oldValue, String newValue){
                    BigDecimal amount = new BigDecimal(newValue);
                    model.setAmount(amount);
                }
        });
        gridPane.add(amountField, 0, 1);

        tipPercentLabel = new Label("Tip Percentage: 15%");
        gridPane.add(tipPercentLabel, 0, 2);
        
        Slider slider = new Slider(0, 100, 15);
        // add listener to slider
        slider.valueProperty().addListener(new ChangeListener<Number>() {
                public void changed(ObservableValue<? extends Number> observable,
                                        Number oldValue, Number newValue){
                    System.out.println(newValue);
                    BigDecimal amount = new BigDecimal(newValue.intValue() / 100.0);
                    System.out.println("Setting percent to: " + amount);
                    model.setTipPercentage(amount);
                }
        });
        gridPane.add(slider, 0, 3);

        Label tipLabel = new Label("Calculated Tip:");
        gridPane.add(tipLabel, 0, 4);
        
        tipField = new TextField("$0.00");
        gridPane.add(tipField, 0, 5);

        Label totalLabel = new Label("Calculated Total:");
        gridPane.add(totalLabel, 0, 6);
        
        totalField = new TextField("$0.00");
        gridPane.add(totalField, 0, 7);

        model = new TipCalculatorModel(new BigDecimal(0.0), new BigDecimal(0.15));
        model.addObserver(this);

        stage.show();
    }

    @Override
    public void update(Observable o, Object arg){
        System.out.println("Update called");
        // update tipPercentLabel
        tipPercentLabel.setText("Tip Percentage: " + PERCENT.format(model.getTipPercentage()));
        //tipPercentLabel.setText("Tip Percentage: " + model.getTipPercentage().multiply(new BigDecimal(100)).intValue());
        // update tipField
        tipField.setText(CURRENCY.format(model.getTip()));
        // update totalField
        totalField.setText(CURRENCY.format(model.getTotal()));
    }
}
