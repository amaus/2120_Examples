import java.math.BigDecimal;
import java.util.Observable;

public class TipCalculatorModel extends Observable {


    private BigDecimal amount;
    private BigDecimal tipPercentage;
    private BigDecimal tip;
    private BigDecimal total;

    public TipCalculatorModel(final BigDecimal amount, final BigDecimal tipPercentage){
        this.amount = amount;
        this.tipPercentage = tipPercentage;
        //refresh();
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getTipPercentage() {
        return tipPercentage;
    }

    public BigDecimal getTip() {
        return tip;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setAmount(final BigDecimal amount){
        if (amount.compareTo(BigDecimal.ZERO) < 0){
            throw new IllegalArgumentException("Amount must be non-negative.");
        }
        this.amount = amount;
        refresh();
    }

    public void setTipPercentage(final BigDecimal tipPercentage){
        if (tipPercentage.compareTo(BigDecimal.ZERO) < 0){
            throw new IllegalArgumentException("Tip percentage must be non-negative.");
        }
        this.tipPercentage = tipPercentage;
        refresh();
    }

    private void refresh(){
        System.out.println("in refresh");
        this.tip = this.amount.multiply(this.tipPercentage);
        this.total = this.amount.add(tip);
        this.setChanged();
        this.notifyObservers();
    }

}
