import java.util.Collection;
import java.util.ArrayList;
/**
 * Created by amaus on 3/21/17.
 */
public class WildcardExamples {
    public static void main(String[] args){
        Collection<Object> c = new ArrayList<Object>();
        c.add("hello");
        c.add(5);
        printCollection(c);

        Collection<Shape> shapes = new ArrayList<Shape>();
        shapes.add(new Square(5));
        shapes.add(new Triangle(4,5));
        calcAllAreas(shapes);
    }

    public static <T> void printCollection(Collection<T> c){
        for(Object e : c){
            System.out.println(e);
        }
    }

    public static <T extends Shape> void calcAllAreas(Collection<T> shapes){
        for(Shape s : shapes){
            System.out.println(s.getArea());
        }
        //shapes.add(new Triangle(4,5));
    }
}
