/**
 * Created by amaus on 3/21/17.
 */
public class StackDriver {
    public static void main(String[] args){
        Stack<Integer> intStack = new Stack<Integer>();
        intStack.push(1);
        intStack.push(2);
        intStack.push(3);
        intStack.push(4);
        intStack.push(5);
        intStack.push(6);
        System.out.println(intStack.pop());
        System.out.println(intStack.pop());
        System.out.println(intStack.pop());
        System.out.println(intStack.pop());
        System.out.println(intStack.pop());
        System.out.println(intStack.pop());

        Stack<Person> unfortunateChildrenStack = new Stack<Person>();
        unfortunateChildrenStack.push(new Person(2,"Sunny"));
        unfortunateChildrenStack.push(new Person(14,"Klaus"));
        unfortunateChildrenStack.push(new Person(16,"Violet"));
        System.out.println(unfortunateChildrenStack.pop());
        System.out.println(unfortunateChildrenStack.pop());
        System.out.println(unfortunateChildrenStack.pop());
    }
}
