/**
 * Created by amaus on 3/16/17.
 */
public class PrintArrays {
    public static void main(String[] args){
        Integer[] intArray = {1, 2, 3, 4, 5};
        Double[] doubleArray = {1.1, 2.2, 3.3, 4.4, 5.5};
        Character[] charArray = {'H', 'E', 'L', 'L', 'O'};

        System.out.println("Values in intArray:");
        printArray(intArray);
        System.out.println("Values in doubleArray:");
        printArray(doubleArray);
        System.out.println("Values in charArray:");
        printArray(charArray);
    }

    public static <T> void printArray(T[] arr){
        for(T element : arr){
            System.out.print(element + " ");
        }
        System.out.println();
    }

    /*public static void printArray(Integer[] arr){
        for(Integer element : arr){
            System.out.print(element + " ");
        }
        System.out.println();
    }

    public static void printArray(Double[] arr){
        for(Double element : arr){
            System.out.print(element + " ");
        }
        System.out.println();
    }

    public static void printArray(Character[] arr){
        for(Character element : arr){
            System.out.print(element + " ");
        }
        System.out.println();
    }*/
}
