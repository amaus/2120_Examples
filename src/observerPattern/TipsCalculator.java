public class TipsCalculator{
    
    public static void main(String[] args){
        double bill = 76.34;
        TipsModel model = new TipsModel(bill);
        TipsTUI view = new TipsTUI(model);

        model.addObserver(view);

        view.displayTipsWelcome(bill);
    }
}
