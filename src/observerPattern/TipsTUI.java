import java.util.Scanner;
import java.util.Observer;
import java.util.Observable;

public class TipsTUI implements Observer {
    private TipsModel tipsModel;

    public TipsTUI(TipsModel m){
        this.tipsModel = m;
    }

    public void displayTipsWelcome( double bill){
        double tipPercentage = .15;
        System.out.printf("Your bill amount is $%.2f.\n", bill);
        System.out.printf("Would you like to tip: \n");
        System.out.println("1) 10%");
        System.out.println("2) 15%");
        System.out.println("3) 20%");
        System.out.println("4) 25%");
        System.out.printf("Enter 1, 2, 3, or 4\n");
        Scanner in = new Scanner(System.in);
        int choice = in.nextInt();
        
        switch(choice){
            case 1:
                tipPercentage = 0.1;
                break;
            case 2:
                tipPercentage = 0.15;
                break;
            case 3:
                tipPercentage = 0.2;
                break;
            case 4:
                tipPercentage = 0.25;
                break;
        }
        tipsModel.calculateTip(tipPercentage);
    }

    public void update(Observable o, Object arg) {
        printTipAmount(tipsModel.getTipAmount());
    }

    public void printTipAmount(double tip){
        System.out.printf("Tip: $%.2f\n", tip);
    }
}
