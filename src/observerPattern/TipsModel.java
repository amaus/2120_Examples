import java.util.Observable;

public class TipsModel extends Observable {
    private double billAmount;
    private double tipAmount;

    public TipsModel(double bill){
        this.billAmount = bill;
    }

    public void calculateTip(double percentage){
        this.tipAmount = this.billAmount * percentage;
        setChanged();
        notifyObservers();
    }
    
    public double getTipAmount(){
        return this.tipAmount;
    }
}
