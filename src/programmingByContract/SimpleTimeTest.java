import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by amaus on 1/26/17.
 */
public class SimpleTimeTest {
    private SimpleTime t;

    @Before
    public void setup(){
        t = new SimpleTime(0,0,0);
    }

    @Test
    public void testSimpleTimeConstruction(){
        assertEquals(t.getHour(),0);
        assertEquals(t.getMinute(),0);
        assertEquals(t.getSecond(),0);
        assertEquals(t.toString(),"00:00:00");
    }

    @Test
    public void testIncrement(){
        t.increment();
        assertEquals(t.getHour(),0);
        assertEquals(t.getMinute(),0);
        assertEquals(t.getSecond(),1);

        SimpleTime t2 = new SimpleTime(13,23,42);
        t2.increment();
        assertEquals(t2.getHour(),13);
        assertEquals(t2.getMinute(),23);
        assertEquals(t2.getSecond(),43);

    }

    @Test
    public void testIncrementSecondsBoundary(){
        SimpleTime t2 = new SimpleTime(0,0,59);
        t2.increment();
        assertEquals(t2.getHour(),0);
        assertEquals(t2.getMinute(),1);
        assertEquals(t2.getSecond(),0);
    }

    @Test
    public void testIncrementMinutesBoundary(){
        SimpleTime t2 = new SimpleTime(0,59,59);
        t2.increment();
        assertEquals(t2.getHour(),1);
        assertEquals(t2.getMinute(),0);
        assertEquals(t2.getSecond(),0);
    }

    @Test
    public void testIncrementHoursBoundary(){
        SimpleTime t2 = new SimpleTime(23,59,59);
        t2.increment();
        assertEquals(t2.getHour(),0);
        assertEquals(t2.getMinute(),0);
        assertEquals(t2.getSecond(),0);
    }
}
