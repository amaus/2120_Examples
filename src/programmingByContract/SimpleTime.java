import java.io.Serializable;

/**
 * Created by amaus on 1/18/17.
 * @author Aaron Maus
 */
public class SimpleTime implements Serializable{
    private int hour;
    private int minute;
    private int second;

    /**
     * This is the constructor for our simple time class.
     *
     * As long as the preconditions on the variables are fulfilled,
     * this constructor will build a valid SimpleTime object.
     *
     * @param hour an int that represents the hour. {@code hour >= 0 && hour < 24}
     * @param minute an int that represents the minute. {@code minute >= 0 && minute < 60}
     * @param second an int that represents the second. {@code second >= 0 && second < 60}
     */
    public SimpleTime(int hour, int minute, int second){
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public void increment(){
        incrementSecond();
    }

    private void incrementSecond(){
        if(getSecond() < 59){
            this.second++;
        } else {
            this.second = 0;
            incrementMinute();
        }
    }

    private void incrementMinute(){
        if(getMinute() < 59){
            this.minute++;
        } else {
            this.minute = 0;
            incrementHour();
        }
    }

    private void incrementHour(){
        if(getHour() < 23){
            this.hour++;
        } else {
            this.hour = 0;
        }
    }

    /**
     * Returns an int representing the current second.
     *
     * @return an int representing the current second.
     */
    public int getSecond(){
        return this.second;
    }

    public int getMinute(){
        return this.minute;
    }

    public int getHour(){
        return this.hour;
    }

    @Override
    public String toString(){
        return (getHour() < 10 ? "0"+getHour() : getHour()) + ":"
                + (getMinute() < 10 ? "0"+getMinute() : getMinute()) + ":"
                + (getSecond() < 10 ? "0"+getSecond() : getSecond());
    }
}
