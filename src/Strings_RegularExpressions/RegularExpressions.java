import java.util.Scanner;

/**
 * Created by amaus on 2/2/17.
 */
public class RegularExpressions {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        String input = "";
        while(true){
            System.out.print("Enter input (or q to quit): ");
            input = in.nextLine();
            if(input.equals("q")){
                System.out.println("BAH-EEEEE!");
                break;
            }
            System.out.println(validate(input) ? "valid" : "invalid");
        }
    }

    public static boolean validate(String text){
        return text.matches("[\\w]*");
    }
}
