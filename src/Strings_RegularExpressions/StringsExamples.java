/**
 * Created by amaus on 1/31/17.
 */
public class StringsExamples {
    public static void main(String[] args){
        String s1 = "hello";
        String s2 = "HELLO";
        String s3 = "hello";
        String s4 = "hi";

        System.out.println("s1: hello");
        System.out.println("s2: HELLO");
        System.out.println("s3: hello");
        // 4 ways to compare: equals(), equalsIgnoreCase(), compareTo(), ==
        if(s1.equals("hello")){
            System.out.println("s1.equals(\"hello\"): true");
        }

        if(s2.equals("hello")){
            System.out.println("s2.equals(\"hello\"): true");
        } else {
            System.out.println("s2.equals(\"hello\"): false");
        }

        if(s2.equalsIgnoreCase("hello")){
            System.out.println("s2.equalsIgnoreCase(\"hello\"): true");
        } else {
            System.out.println("s2.equalsIgnoreCase(\"hello\"): false");
        }

        if(s1 == "hello"){
            System.out.println("s1 == \"hello\": true");
        } else {
            System.out.println("s1 == \"hello\": false");
        }

        if(s1 == s2){
            System.out.println("s1 == s2: true");
        } else {
            System.out.println("s1 == s2: false");
        }

        if(s1 == s3){
            System.out.println("s1 == s3: true");
        } else {
            System.out.println("s1 == s3: false");
        }

        if(s1.equals(s3)){
            System.out.println("s1.equals(s3): true");
        } else{
            System.out.println("s1.equals(s3): false");
        }

        if(s1.equals(s4)){
            System.out.println("s1.equals(s4): true");
        } else{
            System.out.println("s1.equals(s4): false");
        }

        System.out.printf("s1.compareTo(s2): %d", s1.compareTo(s2));
    }
}
