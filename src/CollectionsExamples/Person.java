public class Person implements Comparable<Person> {
    private int age;
    private String surname;

    public Person(int age, String surname){
        this.age = age;
        this.surname = surname;
    }

    public int getAge(){
        return this.age;
    }

    public String getSurname(){
        return this.surname;
    }

    public int compareTo(Person p){
        return this.getSurname().compareTo(p.getSurname());
    }

    public String toString(){
        return getSurname() + ": " + getAge() + " years old";
    }
}
