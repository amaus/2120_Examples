import java.util.ArrayList;
import java.util.Collections;
public class PersonSorter{
    private static ArrayList<Person> list;
    public static void main(String[] args){
        list = new ArrayList<Person>();
        list.add(new Person(34, "Swanson"));
        list.add(new Person(28, "Potter"));
        list.add(new Person(28, "Zebra"));
        list.add(new Person(29, "Granger"));
        list.add(new Person(55, "Obama"));
        list.add(new Person(19, "Bonanno"));
        list.add(new Person(12, "Johnson"));

        printList();

        System.out.println("\nSorting the List by Name...\n");
        //Collections.sort(list);

        printList();

        PersonAgeComparator ageComparator = new PersonAgeComparator();
        
        System.out.println("\nSorting the List by Age...\n");
        Collections.sort(list, ageComparator);

        printList();
    }

    public static void printList(){
        for(int i = 0; i < list.size(); i++){
            System.out.println(list.get(i));   
        }
    }
}
