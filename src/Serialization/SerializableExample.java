import java.io.*;

/**
 * Created by amaus on 2/7/17.
 */
public class SerializableExample {
    private static SimpleTime t1;

    public static void main(String[] args){
        t1 = new SimpleTime(12,31,24);
        writeObjectToFile(t1, "simpleTime.ser");

        SimpleTime t2 = readSimpleTimeFromFile("simpleTime.ser");
        System.out.println(t2);
    }

    public static void writeObjectToFile(Object obj, String fileName){
        ObjectOutputStream out =  null;
        try {

            out = new ObjectOutputStream(new FileOutputStream(fileName));
            out.writeObject(obj);

        } catch(IOException e){

            e.printStackTrace();

        } finally{
            try {
                out.close();
            } catch( NullPointerException e){
                e.printStackTrace();
            } catch( IOException e){
                e.printStackTrace();
            }
        }
    }

    public static SimpleTime readSimpleTimeFromFile(String fileName){
        SimpleTime t = null;
        try(ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName))){
            Object obj = in.readObject();
            if( obj instanceof SimpleTime){
                t = (SimpleTime) obj;
            }
        } catch(FileNotFoundException e){
            e.printStackTrace();
        } catch(IOException e){
            e.printStackTrace();
        } catch(ClassNotFoundException e){
            e.printStackTrace();
        }
        return t;
    }
    
}
