/**
 * Created by amaus on 2/16/17.
 */
public class BinaryTree {
    private int data;
    private BinaryTree leftSubtree;
    private BinaryTree rightSubtree;

    public BinaryTree(int value){
        this.data = value;
        this.leftSubtree = null;
        this.rightSubtree = null;
    }

    public int getData(){
        return this.data;
    }

    public void insert(int value){
        if(value < getData()){ // less than
            if(this.leftSubtree == null){
                this.leftSubtree = new BinaryTree(value);
            } else {
                this.leftSubtree.insert(value);
            }
        } else if(value > getData()){ // greater than
            if(this.rightSubtree == null){
                this.rightSubtree = new BinaryTree(value);
            } else {
                this.rightSubtree.insert(value);
            }
        } else { // equal
            // DO NOTHING. Can not insert data that already exists.
        }
    }

    public void inOrderTraverse(){
        if(this.leftSubtree != null) {
            this.leftSubtree.inOrderTraverse();
        }
        System.out.printf(" %d ", getData());
        if(this.rightSubtree != null){
            this.rightSubtree.inOrderTraverse();
        }
    }
}
