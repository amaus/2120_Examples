public class FibonacciRuntime{
    private static final int SIZE = 31;
    private static long[] runtime = new long[SIZE];
    private static double[] expGolden = new double[SIZE];
    private static double[] expSquare = new double[SIZE];
    private static double[] expCube = new double[SIZE];
    private static double[] goldenToTheN = new double[SIZE];

    public static void main(String[] args){
        calcRuntimes();
        calcExps();
        System.out.printf("F(n):\t# calls\t\tn^1.618\t\tn^2\t\tn^3\t\t1.618^n\n");
        for(int i = 0; i < SIZE; i++){
            System.out.printf("F(%d):\t%d\t\t%.0f\t\t%.0f\t\t%.0f\t\t%.0f\n",i,runtime[i],expGolden[i],expSquare[i],expCube[i],goldenToTheN[i]);
        }
    }
    
    public static void calcRuntimes(){
        runtime[0] = 1;
        runtime[1] = 1;
        for(int i = 2; i < SIZE; i++){
            runtime[i] = runtime[i-1] + runtime[i-2] + 1;
        }
    }

    public static void calcExps(){
        for(int i = 0; i < SIZE; i++){
            expGolden[i] = Math.pow(i, 1.61803398875);
            goldenToTheN[i] = Math.pow(1.61803398875, i);
            expSquare[i] = Math.pow(i, 2);
            expCube[i] = Math.pow(i, 3);
        }
    }
}
