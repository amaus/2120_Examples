/**
 * Created by amaus on 2/14/17.
 */
public class Fibonacci {
    public static void main(String[] args){
        for(int i = 0; i < 100; i++){
            System.out.printf("Fibonacci(%d) = %d\n", i, fibonacci(i));
        }
    }

    public static long fibonacci(long n){
        if(n == 0 || n == 1){
            return n;
        } else {
            return fibonacci(n-1) + fibonacci(n-2);
        }
    }
}
