import java.util.ArrayList;
/**
 * Created by amaus on 2/21/17.
 */
public class BinarySearch {

    public static void main(String[] args){
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(4);
        list.add(7);
        list.add(10);
        list.add(13);
        list.add(17);
        list.add(19);
        list.add(21);
        System.out.printf("Binary Search for %d. Return %d\n",10, binarySearch(list,10));
        System.out.printf("Binary Search for %d. Return %d\n",4, binarySearch(list,4));
        System.out.printf("Binary Search for %d. Return %d\n",19, binarySearch(list,19));
        System.out.printf("Binary Search for %d. Return %d\n",5, binarySearch(list,5));
        System.out.printf("Binary Search for %d. Return %d\n",9, binarySearch(list,9));
        System.out.printf("Binary Search for %d. Return %d\n",20, binarySearch(list,20));
    }

    /**
     * Binary Search takes a sorted list and an Integer and returns
     * the index into the list of that Integer or a negative int
     * if not found
     * @param list the list to search in. The list must be in sorted order
     * @param element the element to search for
     * @return an int representing the index into the list of
     *         the element. A negative int if not found
     */
    public static int binarySearch(ArrayList<Integer> list, Integer element){
        int low = 0;
        int mid = list.size() / 2;
        int high = list.size() - 1;
        return binarySearchHelper(list, element, low, mid, high);
    }

    private static int binarySearchHelper(ArrayList<Integer> list, Integer element,
                                          int low, int mid, int high){
        if(list.get(mid).equals(element)){
            return mid;
        }
        if(low == mid){
            return -1*(mid+1);
        }
        if(element.intValue() < list.get(mid)){ // look on left for element
            high = mid;
            mid = (low + high)/2;
        }
        if(element.intValue() > list.get(mid)){ // look on right for element
            low = mid;
            mid = (low + high)/2;
        }
        return binarySearchHelper(list, element, low, mid, high);
    }
}
