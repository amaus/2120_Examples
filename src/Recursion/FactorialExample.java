import java.math.BigInteger;
/**
 * Created by amaus on 2/9/17.
 */
public class FactorialExample {
    public static void main(String[] args){
        for(int i = 1; i <= 30; i++){
            System.out.printf("Factorial(%d) = %d\n",i,factorial(BigInteger.valueOf(i)));
        }
    }

    public static BigInteger factorial(BigInteger n){
        if(n.equals(BigInteger.ONE)){
            return BigInteger.ONE;
        } else {
            return n.multiply(factorial(n.subtract(BigInteger.ONE)));
        }
    }
}
