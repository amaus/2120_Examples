/**
 * Created by amaus on 2/16/17.
 */
public class BinaryTreeRunner {
    public static void main(String[] args){
        BinaryTree t = new BinaryTree(10);
        t.insert(5);
        t.insert(13);
        t.insert(4);
        t.insert(6);
        t.insert(11);
        t.insert(12);
        t.insert(14);
        t.inOrderTraverse();
    }
}
