
/**
 * Created by amaus on 4/6/17.
 */
public class SinglyLinkedList<T> {
    private SinglyLinkedListNode<T> firstNode;
    private SinglyLinkedListNode<T> lastNode;

    public SinglyLinkedList(){
        this.firstNode = null;
        this.lastNode = null;
    }

    public void insertAtFront(T insertItem){
        if (isEmpty()) {
            firstNode = lastNode = new SinglyLinkedListNode<T>(insertItem);
        } else {
            firstNode = new SinglyLinkedListNode<T>(insertItem,firstNode);
        }
    }

    public void insertAtBack(T insertItem) {
        if(isEmpty()){
            firstNode = lastNode = new SinglyLinkedListNode<T>(insertItem);
        } else {
            lastNode.setNext(new SinglyLinkedListNode<T>(insertItem));
            lastNode = lastNode.getNext();
        }
    }

    public T removeFromFront() throws RuntimeException {
        if(isEmpty()){
            throw new RuntimeException("SinglyLinkedList is empty. Cannot remove from it");
        }

        T removedItem = firstNode.getData();

        if(firstNode == lastNode) {
            firstNode = lastNode = null;
        } else {
            firstNode = firstNode.getNext();
        }

        return removedItem;
    }

    public T removeFromBack() throws RuntimeException {
        if(isEmpty()){
            throw new RuntimeException("SinglyLinkedList is empty. Cannot remove from it");
        }

        T removedItem = lastNode.getData();

        if(firstNode == lastNode) {
            firstNode = lastNode = null;
        } else { // locate the last node
            SinglyLinkedListNode<T> current = firstNode;

            while(current.getNext() != lastNode) {
                current = current.getNext();
            }

            lastNode = current;
            current.setNext(null);
        }
        return removedItem;
    }

    public boolean isEmpty(){
        return (firstNode == null);
    }
}
