import java.util.ArrayList;

/**
 * Created by amaus on 4/6/17.
 */
public class Queue<T> extends SinglyLinkedList<T>{

    public Queue(){
    }

    public void enqueue(T element){
        insertAtBack(element);
    }

    public T dequeue(){
        return removeFromFront();
    }
}
