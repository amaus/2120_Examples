/**
 * Created by amaus on 4/6/17.
 */
public class SinglyLinkedListNode<T> {
    private T data;
    private SinglyLinkedListNode<T> nextNode;

    SinglyLinkedListNode(T object) {
        this(object, null);
    }

    SinglyLinkedListNode(T object, SinglyLinkedListNode<T> node) {
        this.data = object;
        this.nextNode = node;
    }

    public T getData(){
        return this.data;
    }

    public SinglyLinkedListNode<T> getNext() {
        return this.nextNode;
    }

    public void setNext(SinglyLinkedListNode<T> node){
        nextNode = node;
    }
}
