import java.util.ArrayList;
import java.util.EmptyStackException;

/**
 * Created by amaus on 3/21/17.
 */
public class Stack<T> {
    private final ArrayList<T> elements;

    public Stack(){
        elements = new ArrayList<T>();
    }

    public void push(T value){
        elements.add(value);
    }

    public T pop(){
        if(elements.size() <= 0){
            throw new EmptyStackException();
        }
        T value = elements.get(elements.size() - 1);
        elements.remove(elements.size() - 1);
        return value;
    }
}
