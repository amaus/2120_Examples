## In Class Examples

This is the collection of programs that we write in class.

## Using this Repository (through the terminal)

Clone the repository via:

`git clone https://gitlab.com/amaus/1583_001_inClassExamples.git`

As we write more examples in class, I will commit and push them to
this repository. To retrieve them from the server, you will need to
issue the command:

`git pull origin master`

If you edit any of the files before attempting to pull, it may complain
to you about changes that need to be committed. In that case, if you want
to keep your changes, you will want to copy that file to another
location. Then, when back in the repository, you can issue the command

`git stash` 

in order to reset you repository back to the last synced
state. Then you can issue 

`git pull origin master` 

again to pull the
latest changes from the server.

## More info on git

The [atlassian
tutorials](https://www.atlassian.com/git/tutorials/what-is-version-control)
are a good resource for more information on git.
